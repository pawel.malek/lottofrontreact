import React from 'react';
import ReactDOM from 'react-dom';
import AppNavigation from './AppNavigation';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AppNavigation />, div);
  ReactDOM.unmountComponentAtNode(div);
});