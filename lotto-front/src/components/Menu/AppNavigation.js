import React, {useEffect} from 'react';
//import React, {Component} from 'react';
import * as styles from './Menu.module.css';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';
import Home from "../../containers/Home/Home";
import PageOne from "../../containers/PageOne/PageOne";
import NotFound from "../../containers/NotFound/NotFound";

class AppNavigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: 0,
        };
    }

    updateActiveTab = (key) => {
        console.log('KEY: ' + key);
        this.setState({activeTab: key})
    };

    getActiveMenuItemStyle = () => {
        return {
            color: "#008fb5",
            "backgroundColor": "#bfbfbf"
        }
    }

render() {
        const {menuItems} = this.props;
        const {activeTab} = this.state;

        return (
            <Router>
                <div className="MenuBackground">
                    <ul>
                        {
                            menuItems.map((menuItem, index) =>
                                <li key={index}>
                                    <Link key={menuItem.key}
                                          onClick={() => this.updateActiveTab(menuItem.key)}
                                          //style={activeTab === menuItem.key ? this.getActiveMenuItemStyle() : {}}
                                          className={activeTab === menuItem.key ? styles.active : {}}
                                          to={menuItem.url}> {menuItem.name} </Link>
                                </li>)
                        }
                    </ul>
                </div>

                <Switch>
                    <Route exact path={'/'} component={Home} />
                    <Route exact path={'/pageOne'}  component={PageOne} />
                    {/* <Route exact path={'/pageTwo/:idn'} component={PageTwo} />
                                <Route exact path={'/reportView/:idn'} component={Report} />
                                <Route exact path={'/details/:idn'} component={ProductDetails} />*/}

                    <Route render={(props) => (<NotFound {...props} callUpdateActiveTab={this.updateActiveTab} /> )}/>


                </Switch>
            </Router>

        );
    }
}

export default AppNavigation;
