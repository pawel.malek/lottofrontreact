import React from 'react';
import ReactDOM from 'react-dom';
import LottoResultData from './LottoResultData';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<LottoResultData />, div);
  ReactDOM.unmountComponentAtNode(div);
});