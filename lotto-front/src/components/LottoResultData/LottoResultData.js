import React from 'react';
import PropTypes from 'prop-types';
import styles from './LottoResultData.module.css';

const LottoResultData = props => (
    <div key={props.index} className={styles.container}>
        <p>id: {props.index}</p>
        <p>sum: {props.lottoData.sumOfNumbers}</p>
        <p>numbers: {props.lottoData.numbersAsString}</p>
    </div>
);

LottoResultData.propTypes = {};

LottoResultData.defaultProps = {};

export default LottoResultData;
