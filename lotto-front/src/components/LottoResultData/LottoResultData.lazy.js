import React, { lazy, Suspense } from 'react';

const LazyLottoResultData = lazy(() => import('./LottoResultData'));

const LottoResultData = props => (
  <Suspense fallback={null}>
    <LazyLottoResultData {...props} />
  </Suspense>
);

export default LottoResultData;
