import logo from './logo.svg';
import React, {useEffect} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import AppNavigation from "./components/Menu/AppNavigation";
import NotFound from "./containers/NotFound/NotFound";
import Home from "./containers/Home/Home";
import PageOne from "./containers/PageOne/PageOne";
import {menuRoutes} from "./helpers/MenuRoutes";

function App() {

    return (
        <div>
            <AppNavigation key="1" menuItems={menuRoutes} />
        </div>
    );
}

export default App;
