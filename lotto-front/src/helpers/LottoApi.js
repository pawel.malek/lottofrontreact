import * as api from './RestApi'
import { urlForBigLotto } from './Routes'

export const getNumbers = () =>
    api.get(urlForBigLotto());

export const getNumbersWithIterations = (iterations) =>
    api.get(urlForBigLotto(iterations));
