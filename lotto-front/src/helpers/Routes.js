const serverUrl = 'http://localhost:8181/api/lotto';

export const urlForBigLotto = iterations =>
    iterations ? `${serverUrl}/big/${iterations}` : `${serverUrl}/big/`;
