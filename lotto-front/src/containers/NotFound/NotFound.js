import React from 'react';
import * as styles from './NotFound.module.css';
import {Redirect} from 'react-router'

class NotFound extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            counter: 10
        };
    }

    componentDidMount = () => {
        const intervalId = setInterval(this.countdown, 1000);
        this.setState({intervalId});
    };

    countdown = () => this.setState({counter: this.state.counter - 1});

    componentWillUnmount = () => {
        clearInterval(this.state.intervalId);
        this.callUpdateActiveTab();
    }

    callUpdateActiveTab = () => {
        this.props.callUpdateActiveTab(0)
    }

    render() {
        const { location } = this.props;
        const { counter } = this.state;
        return (
            <div>
                <h1>404 - PAGE NOT FOUND</h1>
                <p>{location.pathname}</p>
                <p>Redirect to homepage in {counter} seconds</p>
                { counter === 0 && <Redirect to={'/'} /> }
            </div>
        );
    }
}

export default NotFound;

