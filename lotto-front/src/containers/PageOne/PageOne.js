import React from 'react';
import * as styles from './PageOne.module.css';
import * as api from "../../helpers/LottoApi";
import LottoResultData from "../../components/LottoResultData/LottoResultData";

class PageOne extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            numbers: []
        }
        console.log('Hello from PageOne constructor');
    }

    async componentDidMount() {
        const content = await api.getNumbersWithIterations(5);
        this.setState({numbers: content});
        console.log("Numbers Refreshed");
}

    render() {
        const {numbers} = this.state;
        console.log(numbers);
        return (
            <div className={styles.pageHolder}>
                {
                    numbers.map((item, index) =>
                        <LottoResultData lottoData={item} index={index} />
                    )
                }
            </div>
        );
    }

}

export default PageOne;
