import React, { lazy, Suspense } from 'react';

const LazyPageOne = lazy(() => import('./PageOne'));

const PageOne = props => (
  <Suspense fallback={null}>
    <LazyPageOne {...props} />
  </Suspense>
);

export default PageOne;
