import React from 'react';
import styles from './Home.module.css';

export default function Home() {
    return (
        <div className={styles.home}>
            <div><h1>HOME</h1></div>
        </div>
    );
}
