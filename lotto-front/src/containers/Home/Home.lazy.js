import React, { lazy, Suspense } from 'react';

const LazyHome = lazy(() => import('./Home'));

const home = props => (
  <Suspense fallback={null}>
    <LazyHome {...props} />
  </Suspense>
);

export default home;
